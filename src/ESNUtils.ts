import ESN from './ESN';

export const isStarted = (entreprise: ESN) => entreprise.started;
// eslint-disable-next-line no-console
export const afficherEtat = (entreprise: ESN) => console.log(isStarted(entreprise));

export default {
  isStarted,
  afficherEtat,
};
