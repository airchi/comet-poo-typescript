interface ESN {
  nom: string;
  adresse: string;
  effectif: number;
  started: boolean;
}

export default ESN;
