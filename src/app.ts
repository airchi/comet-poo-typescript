import ESN from './ESN';
import { afficherEtat } from './ESNUtils';

const extia: ESN = {
  nom: 'EXTIA',
  adresse: 'Rue Regnault, 86',
  effectif: 500,
  started: true,
};

const logware: ESN = {
  nom: 'LOGWARE',
  adresse: 'Rue Nationale, 150',
  effectif: 50,
  started: true,
};

afficherEtat(extia);
afficherEtat(logware);
